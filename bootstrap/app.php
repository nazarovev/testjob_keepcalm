<?php

use DI\Container;
use DI\Bridge\Slim\Bridge as SlimAppFactory;
use \App\Providers\ServiceProvider;

$app = SlimAppFactory::create(new Container);

ServiceProvider::setup($app, config('app.providers'));
//dd(config('middleware'));

//$middleware = require __DIR__ . '/../app/middleware.php';
//$middleware($app);

//$routes = require __DIR__ . '/../app/routes.php';
//$routes($app);

return $app;