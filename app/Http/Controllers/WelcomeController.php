<?php

namespace App\Http\Controllers;

use App\Support\View;

class WelcomeController
{
    public function index(View $view)
    {
        return $view('auth.home',[
            'name' => 'Clean Code Studio'
        ]);
    }

    public function show(View $view, $name, $id)
    {
        return $view('user.show',compact('name','id'));
    }
}