<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/*
 * Autoload global dependencies ans auto-loading local dependencies via use
 */
require __DIR__ . '/../vendor/autoload.php';

/*
 * Boot up application, AKA Turn the light on.
 */
$app = require __DIR__ . '/../bootstrap/app.php';

/*
 * Passing our Request through the app
 */
$app->run();